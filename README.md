A puzzle game, written using Unreal Engine 4 and C++
The goal is to find a way to hold the door open and escape the room.

Demo screenshots of the progress so far:

![Alt text](./Screenshots/Screenshot_426.png?raw=true)
![Alt text](./Screenshots/Screenshot_427.png?raw=true)