// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapeRoom.h"
#include "PositionReporter.h"


// Sets default values for this component's properties
UPositionReporter::UPositionReporter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPositionReporter::BeginPlay()
{
	Super::BeginPlay();

	const FString componentName = GetOwner()->GetName();
	const FString position = GetOwner()->GetActorTransform().GetLocation().ToString();
	UE_LOG(LogTemp, Warning, TEXT("%s is at : %s"), *componentName, *position);

	objectLocation = GetOwner()->GetActorTransform().GetLocation();
	dir = 1.0f;
}


// Called every frame
void UPositionReporter::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	if (objectLocation.X >= 200.f)
	{
		dir = -1.0f;
	}
	
	if (objectLocation.X <= 0.0f)
	{
		dir = 1.0f;
	}
	//UE_LOG(LogTemp, Warning, TEXT("objectLocation %f"), objectLocation.X);
	//UE_LOG(LogTemp, Warning, TEXT("dir %f"), dir);

	FTransform transform = GetOwner()->GetActorTransform();

	//transform.AddToTranslation(objectLocation);
	objectLocation += FVector(1.f, 0.f, 0.f) * DeltaTime * 250.f * dir;
	transform.SetTranslation(objectLocation);
	//

	GetOwner()->SetActorTransform(transform);
}

