// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapeRoom.h"
#include "EventTrigger.h"


// Sets default values for this component's properties
UEventTrigger::UEventTrigger()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	pressurePlate = nullptr;
	player = nullptr;
	rotationAngle = 90.0f;
	doorCloseDelay = 1.0f;
}


// Called when the game starts
void UEventTrigger::BeginPlay()
{
	Super::BeginPlay();

	player = GetWorld()->GetFirstPlayerController()->GetPawn();
	owner = GetOwner();
}


// Called every frame
void UEventTrigger::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	
	if (pressurePlate->IsOverlappingActor(player))
	{
		RotateActor(owner, 0.0f, rotationAngle, 0.0f);
		doorLastOpenTime = GetWorld()->GetTimeSeconds();
	}

	if (GetWorld()->GetTimeSeconds() - doorLastOpenTime > doorCloseDelay)
	{
		RotateActor(owner, 0.0f, 0.0f, 0.0f);
	}
}

void UEventTrigger::RotateActor(AActor* target, const float pitch, const float yaw, const float roll)
{
	FRotator rotation(pitch, yaw, roll);
	target->SetActorRotation(rotation);
}

