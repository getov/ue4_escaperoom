// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <tuple>

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEROOM_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

private:

	// in cm
	float debugRayLength;

	UPhysicsHandleComponent* physicsHandle;

	UInputComponent* inputComponent;

	std::tuple<FVector, FVector> GetLineTraceStartEnd();

	// Cast a ray from the player's viewpoint and get the first intersected object
	FHitResult GetFirstLineTraceHit();

	void InitPhysicsHandle();

	void InitInputComponent();

	// Input events
	void EventGrab();
	void EventRelease();
};
