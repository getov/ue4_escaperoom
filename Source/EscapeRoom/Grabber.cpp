// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapeRoom.h"
#include "Grabber.h"


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	debugRayLength = 100.0f;
	physicsHandle = nullptr;
	inputComponent = nullptr;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	InitPhysicsHandle();

	InitInputComponent();
}


// Called every frame
void UGrabber::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// if we have hit an object and grabbed it, do the update
	if (physicsHandle->GrabbedComponent)
	{
		std::tuple<FVector, FVector> lineTraceStartEnd = GetLineTraceStartEnd();
		physicsHandle->SetTargetLocation(std::get<1>(lineTraceStartEnd));
	}
}

std::tuple<FVector, FVector> UGrabber::GetLineTraceStartEnd()
{
	FVector playerViewPoint;
	FRotator playerRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(playerViewPoint, playerRotation);
	FVector lineTraceEnd = playerViewPoint + playerRotation.Vector() * debugRayLength;

	return std::make_tuple(playerViewPoint, lineTraceEnd);
}

FHitResult UGrabber::GetFirstLineTraceHit()
{;
	std::tuple<FVector, FVector> lineTraceStartEnd = GetLineTraceStartEnd();

	// Ray-trace and check for collisions with objects that has ECC_PhysicsBody channel
	FCollisionQueryParams traceParams(FName(TEXT("")), false, GetOwner());
	FHitResult lineTraceHit;
	GetWorld()->LineTraceSingleByObjectType(
		lineTraceHit, 
		std::get<0>(lineTraceStartEnd), 
		std::get<1>(lineTraceStartEnd),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), traceParams
	);

	return lineTraceHit;
}

void UGrabber::InitPhysicsHandle()
{
	physicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (!physicsHandle)
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing PhysicsHandle component"), *GetOwner()->GetName());
	}
}

void UGrabber::InitInputComponent()
{
	inputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (inputComponent)
	{
		inputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::EventGrab);
		inputComponent->BindAction("Grab", IE_Released, this, &UGrabber::EventRelease);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing Input component"), *GetOwner()->GetName());
	}
}

void UGrabber::EventGrab()
{
	if (physicsHandle)
	{
		FHitResult lineTraceHit = GetFirstLineTraceHit();
		AActor* intersectedObject = lineTraceHit.GetActor();

		if (intersectedObject)
		{
			// attach physics handle
			physicsHandle->GrabComponentAtLocationWithRotation(
				lineTraceHit.GetComponent(),
				NAME_None,
				lineTraceHit.GetComponent()->GetOwner()->GetActorLocation(),
				lineTraceHit.GetComponent()->GetOwner()->GetActorRotation()
			);
		}
	}
}

void UGrabber::EventRelease()
{
	if (physicsHandle)
	{
		physicsHandle->ReleaseComponent();
	}
}

